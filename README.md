Website
=======

このレポジトリは https://www.clear-code.com の内容を管理します。

| #            | URL                         |
| ------------ | --------------------------- |
| 本番サイト   | https://www.clear-code.com  |
| ステージング | https://clear-code.gitlab.io/website |
| GitLab       | https://gitlab.com/clear-code/website |

開発情報
--------

### ローカルビルドの構築方法

```bash
$ git clone https://gitlab.com/clear-code/website
$ cd website
$ bundle install --path .b
$ bundle exec rake build
```

ビルド済みのコンテンツは`_site`配下に出力されます。

### ステージング環境への反映

ブランチを切って、GitLabにプッシュすると、自動的に内容がステージング環境に反映されます。

* 例えば`test`ブランチをプッシュすると https://clear-code.gitlab.io/website/test に公開されます。

### 本番環境への反映

GitLabに`master`ブランチにプッシュすると、自動的に www.clear-code.com にデプロイされます。

個別ページの管理
----------------

### プレスリリースの更新

1. `YYYYMMDD-title${拡張子}`というファイル名でプレスリリースを起稿します。`${拡張子}`は`.md`推奨です。（`.html`も可）
2. 作成したファイルを`/press-releases/`に追加します。
3. `_data/services.yaml`にエントリを追加します。2021年8月現在は、リンク先のURLの拡張子は`.html`になります。`.md`でプレスリリースを作っても最終的に`.html`に変換するためです。

実際の例: 452f86cee

### トピックの更新

1. `YYYYMMDD-title${拡張子}`というファイル名でトピックを起稿します。`${拡張子}`は`.md`推奨です。（`.html`も可）
2. 作成したファイルを`/topics/`に追加します。
3. `_data/topics.yaml`にエントリを追加します。2021年8月現在は、リンク先のURLの拡張子は`.html`になります。`.md`でプレスリリースを作っても最終的に`.html`に変換するためです。

実際の例: 018b7ee65

### ククログの投稿

詳細は[ククログの記事の書き方](https://www.clear-code.com/blog/2021/5/28/how-to-write-article.html)を参照。

1. `YYYY-MM-DD-title.md`というファイルにブログ記事を書きます。
2. 作成したファイルを`/_post/`に追加します。
3. `_config.yaml`に（まだ未登録であれば）作者やタグの定義を追加します。
    * タグを新規に追加する場合は、`/blog/{tag}/index.html`ファイルも作成します。タグをクリックしたときのページに必要です。

実際の例

* 9d1d54785
* 41d1a45a (タグを新規追加)

画像の保存
----------------

### いろいろなところで使用する画像の登録

1. `/images/`に画像を登録。

### 記事に紐づく画像の登録
1. 特定記事と同名のファルダを記事の入っている場所に作成。
2. フォルダ内に画像を登録します。

例：　
```
press-release
├── 20210113-redmine.html
├── 20210113-redmine
│   └── redmine.png
├── 20210325-system-admin-girl.html
├── 20210325
│   └── bookcover.jpg
...
```
### 画像のライセンスに関する注意事項
クリアコードが著作者ではない画像あるいはクリアコードがデフォルトで設定しているライセンスでないコンテンツについては、[ライセンスページ](license/index.html)にも情報を追記するようにしてください。

### サイトからリンクを貼るときの注意事項
* サイト内のページにリンクする場合は、ステージング環境でリンクが自動的に解決されるよう、`{% link ～ %}` を使って `[リンクテキスト]({% link path/to/file %})` のようにします。（たとえば、[サービスのトップページ]({% link services/index.md %})であれば `[サービス]({% link services/index.md %})` です。）
  * **2021年11月9日時点の注記：** ただし、[ククログのトップページ]({{ "blog/" | relative_url }})だけはこの方法でリンクするとエラーになります。代替として、 `[リンクテキスト]({{ "blog/" | relative_url }})` と書く必要があります。関連していそうなissueとpull request：[sverrirs/jekyll-paginate-v2#104](https://github.com/sverrirs/jekyll-paginate-v2/issues/104)/[sverrirs/jekyll-paginate-v2#226](https://github.com/sverrirs/jekyll-paginate-v2/pull/226)
