---
layout: default
main_title: 会社情報
type: about
---

<h2 class='text-center'>会社概要</h2>

<table>
  <tbody>
    <tr>
      <th>会社名</th>
      <td>株式会社クリアコード</td>
    </tr>
    <tr>
      <th>役員</th>
      <td>
        <dl>
          <dt>代表取締役</dt>
          <dd>須藤　功平</dd>
        </dl>
        <dl>
          <dt>取締役</dt>
          <dd>足永　拓郎</dd>
        </dl>
        <dl>
          <dt>取締役</dt>
          <dd>南　慎一郎</dd>
        </dl>
        <dl>
          <dt>取締役</dt>
          <dd>藤本　誠二</dd>
        </dl>
      </td>
    </tr>
    <tr>
      <th>従業員数（役員・アルバイトを含む）</th>
      <td>9名</td>
    </tr>
    <tr>
      <th>会社設立</th>
      <td>2006年7月25日</td>
    </tr>
    <tr>
      <th>所在地</th>
      <td>〒359-1111
        <br>埼玉県所沢市緑町二丁目6番5号 芝﨑ビル103
      </td>
    </tr>
    <tr>
      <th>交通アクセス</th>
      <td>
        西武新宿線新所沢駅 徒歩3分
      </td>
    </tr>
    <tr>
      <th>TEL/FAX</th>
      <td>04-2907-4726</td>
    </tr>
    <tr>
      <th>資本金</th>
      <td>975万円</td>
    </tr>
    <tr>
      <th>事業内容</th>
      <td>ソフトウェア開発</td>
    </tr>
    <tr>
      <th>取引銀行</th>
      <td>三井住友銀行、朝日信用金庫</td>
    </tr>
    <tr>
      <th>決算公告</th>
      <td>決算公告は<a href="{{ "/koukoku" | relative_url }}">こちら</a></td>
    </tr>
  </tbody>
</table>

[会社紹介資料（PDF）をダウンロード]({% link about/company-profile.md %})

<h2 class='text-center'>理念</h2>

<p>
クリアコードの理念は、<b>フリーソフトウェア</b>と<b>ビジネス</b>の両立です。

<p>
当社の目的は、単に会社を継続していくことではありません。フリーソフトウェアの開発で学んだことを継続的にビジネス分野に活用していくことで会社を継続し、それと同時に、ビジネスを継続することでフリーソフトウェアへ継続的にコミットメントしていくこと、この両立の実現が当社の目的です。この理念は、我々がフリーソフトウェアの開発で学んだことがベースとなっています。

<p>
どのようにそれを実現するかの取り組み方については、<a href='https://www.clear-code.com/blog/2015/9/17.html'>クリアコードとフリーソフトウェアとビジネス</a>という記事で詳しくご紹介しています。

<h2>開発</h2>

<p>
開発はクリアコードの核となる活動です。そのため、「どのように開発するか」がクリアコードという会社がどのような会社となるかを大きく左右します。クリアコードでは次のように開発しています。このような開発を積み重ねてクリアコードの理念を実現しています。

<ul class='link_list grid-x grid-padding-x'>
  <li class='cell medium-6 large-4'><a href='{{ "/philosophy/development/style.html" | relative_url }}'>開発スタイル</a>
</ul>

<h2>沿革</h2>
<table>
 <tbody>
 <tr>
 <th>2006年 7月</th>
 <td>株式会社グッデイに勤務していた5名が独立し、株式会社クリアコードを設立。</td>
 </tr>
 <tr>
 <th>2006年 8月</th>
 <td>IPAオープンソースソフトウェア活用基盤整備事業「Linux環境における外字管理システムの仕様開発とプロトタイプ作成」に参加。</td>
 </tr>
 <tr>
 <th>2006年 9月</th>
 <td>有限責任中間法人Mozilla Japanのサポートパートナーに登録。
 <br><a href='{% link services/mozilla/menu.html %}'>Firefox Thunderbirdのサポートサービスを開始。</a></td>
 </tr>
 <tr>
 <th>2008年 8月</th>
 <td>IPAオープンソフトウェア利用促進事業「迷惑メール対策ポリシーを柔軟に実現するためのmilterの開発」に採択され、2009年4月に<a href='{% link services/milter-manager.html %}'>milter manager 1.0.0を開発・リリース。</a></td>
 </tr>
 <tr>
 <th>2008年 8月</th>
 <td>須藤 功平が代表取締役に就任。</td>
 </tr>
 <tr>
 <th>2008年 8月</th>
 <td>全文検索エンジンSennaの開発に参加。<a href='{% link services/groonga.md %}'>Groongaの開発につながる。</a></td>
 </tr>
 <tr>
 <th>2009年 6月</th>
 <td>NICT「先進技術型研究開発助成金」に採択され、組み込み向けブラウザの研究開発を実施。組み込み開発に着手。</td>
 </tr>
 <tr>
 <th>2010年 8月</th>
 <td>取引先3社より出資を受ける。</td>
 </tr>
 <tr>
 <th>2015年 8月</th>
 <td><a href='{% link services/fluentd.md %}'>Fluentdの開発に参加。</a></td>
 </tr>
 <tr>
 <th>2015年12月</th>
 <td><a href='{% link services/browser-customize.md %}'>Gecko Embeddedプロジェクトに参加。</a></td>
 </tr>
 <tr>
 <th>2016年 7月</th>
 <td><a href="{% post_url 2016-07-27-index %}">設立10周年。</a></td>
 </tr>
 <tr>
 <th>2016年12月</th>
 <td><a href='{% link services/apache-arrow.md %}'>Apache Arrowの開発に参加。</a></td>
 </tr>
 <tr>
 <th>2018年11月</th>
 <td>株式会社セナネットワークスと資本提携。</td>
 </tr>
 <tr>
 <th>2019年11月</th>
 <td><a href='{% link services/browserselector.html %}'>ブラウザ切替ツールBrowserSelectorを開発・リリース。</a></td>
 </tr>
 <tr>
 <th>2020年12月</th>
 <td>埼玉県所沢市に本社移転。</td>
 </tr>
 <tr>
 <th>2021年 7月</th>
 <td>設立15周年。</td>
 </tr>
 </tbody>
</table>

