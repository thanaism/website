---
layout: default
main_title: 用語集
sub_title:
toc: true
---

## English Technical Term That have Different Meaning from Regular Meaning

英単語がそのままカタカナ表示され、日英共通の使い方をされているものの、一般的な英単語としての意味が分かっていると、英文中で混乱しがちな単語もしくは表現。

元の英単語の意味から察することはできたり、できなかったりする。技術用語的には、単語以外の要素が含まれているアクションになっていることが多い。


### Fork

カラトリーのフォークのこと。技術用語になると、[Git](#git)のリポジトリー内で行われる分岐させるアクションのこと。通常使う言葉で似ている言葉だと「split」とか（分岐する）。動詞的にはforkで分岐するという意味もあるので、まだわかりやすい。

### Bug

通常は、虫のこと。他の言い方をするとInsectやそれ以外にも、もぞもぞする小さい虫などのこと。　技術用語になると、コンピューターやシステムに現れる欠陥のこと。はるか昔、本当に虫がハードウェアに侵入して欠陥がおきたことから。通常使う言葉で説明するなら「fault」「defect」だとわかりやすい。

### Chip

おやつ。ポテトチップやトルティーヤチップなどのこと。技術用語でいえば、パソコンの内部の部品のこと。もう一点日本語で「チップ」というと英語の「Tip＝心づけ」のこともある。なお、「Tip」には心づけのほかに、助言やヒントという意味もある。

### Lint

糸くず。布。技術用語になると、間違いがないかコードをチェックするツールのこと。人がやるより、早くてミスが減る。

### Log

丸太。もしくは、意図的に記録を付けている日誌のようなもののこと。技術用語の場合は、設定次第で取得が可能な機械の動作履歴の意味。似ているようでちょっと異なる。（例） 「Take a log」が「日報つけなさい。」のように感じるが、「履歴を取得しなさい。」の意図になる。

### Patch

小さい布、斑、畑の一区画（Pumpkin Patch＝かぼちゃ畑）。技術用語的にはプログラムの変更箇所。あて布的な意味だけど、布ではなくプログラムのこと。

### Shell

殻。卵や、植物などの外側の硬いところ。技術用語的にはプログラムの一種。使っている人の操作を受け付けてソフトウェアに指示したり、ソフトウェアからの返答を利用者に表示したりする部分を担っている。指示を出すコマンドそのものは Shell Scriptになる。

### Anchor

船のいかり、テレビ番組のメインキャスター。技術的には、「アンカータグ」「アンカーリンク」の略称的に「アンカー」といわれている。HTMLなどに記述されているとアンカーしている場所に飛んで表示が切り替わる。

### Library

図書館。蔵書のコレクション。技術的には、いろいろな機能の既存プログラムのファイルを再利用できる形でまとめたもの。いろいろな種類がある。これを再利用することで作りたいプログラムの作りたい部分だけに集中できるようになる。 誰かがプログラムの一部を再利用するために切り出したらそれがライブラリーになる。

### Template

鋳型。定型書式。ひな型。技術的には、いろいろなものを簡単に導入できるように準備してある状態になっていること。テンプレートががあれば、それに必要な事項を穴埋めして使えるようになる。

### Document

書類。資料。文書のこと。技術的には、図や説明を含んだファイルやデータのことを指す。また、技術や製品についての詳しい説明や、機能、仕様、注意事項がまとめてある資料や文書の総称のこと。Documentのなかには、初歩を伝えるTutorial、 詳細を記載してあるReference manual、　困ったときのTrouble　shoot、最新情報を伝えるRelease noteなど様々なものがある。

### Cookbook

料理本。料理のレシピの集まった本。技術的には、ざっと概要を把握してやりたいことをやるための手順や材料となるプログラムがまとまっているレシピ集のこと。Reference manual（取扱説明書）のように詳細な説明ではないけれど、自分のやりたいことに対してできるかできないかや、どうやるかを知ることができる[ドキュメント](#document)の一種。

### Source

源。根源。情報源。日本語で技術的にソースというときは、英語の「Source code」や「Source Program」を意図していることが多い。人間が理解できる形で記述されているプログラムのこと。日常的な使い方でSource単独というと、技術的なソースコードの意図にはならず、情報源＝参照している情報という意図になる。カタカナの「ソース」には「たれ」の意味もあるが、それは「Sauce」で綴りが違う。

### Git

「ばかもの」という呼びかけ。あまり常用しない。いろいろな理由から、技術的にはバージョン管理システムのことを言う。プロジェクトに対して行われた変更内容や変更したユーザーの記録などを、都度保存する仕組み。https://git-scm.com/
インターネット上にメインのバージョンを保管しておけるいろいろなサービスがある。GitHubやGitLabが有名。

### Engine

燃料をエネルギーに変えて物理的動力を生み出す機構。エンジン。車などに搭載されるエンジンが元の語源、技術的には、何らかの大きな処理を行うために必要ないろいろな要素のプログラムがまとまった状態のもの。例えば、よく使われている検索エンジンの場合、検索するための予備動作（データベースの作成や、インデックスの設定）のプログラム部分、キーワードを探してくることをするプログラム部分などをまとめて意図する。

### Thread
糸。（話の）筋道。ねじ山。プログラミングで使うときは、同時に実行されない処理の単位。同じスレッド内では同時に複数の処理は実行されないが、異なるスレッドでは並列にそれぞれ別の処理が実行されうる。それと同時に、話の筋道という意味から、インターネット界隈ではチャットや会話の一連の流れの一塊のこともスレッドもしくはスレといわれる。


## Technical Terms

技術の世界で使われている言葉の説明。

### CI

Continuous integration

ソフトウェア開発をしているときに、コードをコミットするごとに自動的にテストを実行して壊れていないかを確認するプロセスのこと。CIのプロセスを提供するサービスはいろいろあり、ソフトウェア開発ごとに適切なサービスを使う。対応するOSや、テストの内容によって使用するプログラムが変わる。また実行頻度なども自動で設定してあるものや、都度実行させるものなど様々。以前は時間がかかるものだったが、処理速度が速くなったことで、小分けに頻度を高く実行することも可能になった。

### ビルドシステム

プログラムを環境に合わせてビルドすることを自動化するシステム。

関連するソフトウェアのアップデートにより、ビルドシステムで出来上がったプログラムが壊れたりする。その時は原因を究明して、出来上がったプログラムを直したり、ビルドシステム自体を直したりする必要がある。

### プログラミング言語のバージョン

プログラミング言語は、規格があって、バージョンごとにできることが変わる。 結果どのバージョンでどう実装するか、が変わる。

バージョンの表記の方法は個別言語に依存する。C++だったらC++17などそのあと数字がついていて判別が可能。

### ディレクトリーとフォルダ

意図していることは同じことが多い。ファイルを分類して入れておく所(ハードウェアの記録場所)を意図する。視覚的に操作するGUI(Graphical User Interface)の時はフォルダ、文字入力で操作するCUI(Character User Interface)の時はディレクトリということが多い。フォルダには、保管しておくところという以外にもメニュー画面的な機能があるときもある。（記録場所ではない。）

### レンダリングエンジン

文字の指示を画面上で見せたい形に見せるための様々なプログラムが一塊になっている仕組みのこと。

なじみがあるものでは、Webブラウザの[エンジン](#engine)。ホームページなど「HTML」形式で文字ばかり表記されたソースを、よく見かけるような見出しや画像、テキストや、アニメーションなどに変換してユーザーに表示している。

### アップデートとリリース

ソフトウェアの更新版が出たときに使われる言葉。起こっている事象としてはほぼ同じこと。発信する側が、ソフトウェアを開発している側だと「更新版をリリースする」になり、ユーザー側だと「更新版にアップデートする」という感じ。ただ、リリースが意図するのは、「新しい商品やサービスの発表」というときもあるので、紛らわしい。英語の語源と日本語の持つ意味がずれていっているパターンかもしれない。

### カーネル

Kernel。種の中身。アイディアの芯になる部分のこと。コンピュータを動かすための基本的な動作をつかさどるプログラムの塊のこと。英語の意味としては分かりやすい。フライドチキンで有名な人を「カーネル」というが、こちらは綴りがColonelなので全く関係ない。 

### プログラミングとコーディング

技術的な知識がないとその差が分かりづらい言葉。コーディングは、自然言語といわれる人が使う言葉を、機械が理解できる形に書き換えること。プログラミングは人が使う言葉を機械が理解できる形に書き換えた上で、機械がきちん動くかどうかを試し、必要な情報や指示を追加して、求められる挙動をおこなう状態にすること。プログラミングの一部にコーディングがあるといえる。
日本のでウェブサイトの制作などでコーディングといった場合は、文字のサイズや、画像などをデザインされた状態にレンダリングエンジンが表示できるようにファイルを作成することを指していることが多い。この認識の違いによりいわゆる「プログラミング言語」が何を指すのか、少しわかりづらいのではないかと思われる。

### 人工言語

人が、人同士で使う言葉「自然言語」に対して、機械とコミュニケーションをとるために作られた言葉。人工言語はそれぞれの目的によって、さらに分類することができる。
* プログラミング言語：　機械への指示だしに使われている言語。C、Ruby、Pythonなど。
* スタイルシート言語:　文書やwebサイトなどにおいて、文字列全体に関する見た目（文字種、文字サイズ、レイアウトなど）に関する指示をまとめて指示するための言語。CSSなど。
* マークアップ言語：文字に指示を付けるのに使う言語。個別の文字に対して、見出しにしたり、大文字にしたりする指示をだす。HTML、Markdownなど。

技術のことが関わらない文書などでは、まとめて「プログラミング言語」という言葉で表記されているように思われることがある。それぞれの言語が、さらに細かく種類が分かれていくのは、自然言語を分類するときに、ゲルマン系言語に英語・ドイツ語、ラテン系言語にフランス語・スペイン語みたいになるのと同じ。

### パス

技術的によく使うのは`path`をカタカナ語にした「パス」のこと。pathのもともと持つ「道筋」というのと同じような意味合いでプログラムがたどる道筋・経路を示したもの。相対パスや絶対パスといった言い方で、コマンド操作を行うときには必須の知識。一方で、外来語として日本に定着しているのは、名詞の入場券・動詞の手渡すを意味する`pass`のことだったり、「パスワード」を略した「パス」だったりするので、紛らわしい。


## Technical Products/Service Terms

ソフトウェアそのものや、サービスなど固有名詞に近いもの。

### Crossbow

https://arrow.apache.org/docs/developers/crossbow.html

Apache Arrowの開発で使われているパッケージ関連・インテグレーションテスト関連の作業を自動化するツール。

毎日定義済みのテストを実行して結果を開発者に報告している。定期的ではなく、指示をだして都度テストを実行することもできる。Arrowに引っ掛けて名づけられている。

### Boost

C++言語用のライブラリーのひとつ。

https://github.com/boostorg/boost

便利機能がいろいろつまっている。オープンソースで日々進化している。 多くの要素で構成されており、それぞれの保存場所が変わることがある。依存関係があるので、変更するときは依存するいろいろなものも同時に変更しなくてはいけない。 使っているバージョンが古くなると対応していないことがある。英単語における「Boost」は動詞なので、使用箇所が違うため、英文のなかで分かりづらくなることはあまりない。

### Fluentd

拡張性の高いログ収集OSS。

技術の進化により顧客の動きや、ソフトウェアの動作履歴など様々な[ログ](#log)が集められるようになった現代で、ログデータをより有効に活用し、分析などを可能とするデータ連携をしてくれる。データを連携させることで、より効率的な分析や解析を可能にできる。1000以上のプラグインで、様々なサービスとのデータ連携を実現。Cloud Native Computing Foundation (CNCF) により認定されたプロジェクトの一つ。

https://www.fluentd.org/

### CMake

[ビルドシステム](#ビルドシステム)の一種。オープンソースのクロスプラットフォームでビルドやテストやパッケージをできるようにするのをサポートするツール。

https://cmake.org/

ソフトウェアを環境に合わせてカスタマイズするにあたり、それぞれの環境にどんな違いがあって、プログラムを動かすためにどんな変更が必要かを探してくれる。CMakeで調べた変更条件をもとに、ほかのツールを使ってビルドして、ほかの環境に対応したプログラムを作ることができる。多種多様な依存関係があるので、それぞれの環境の情報をきちんととれるようにしておかないといけない。

### Blink

Webブラウザの[レンダリングエンジン](#レンダリングエンジン)の一種。

KHTMLというものから進化してきたレンダリングエンジン。近年、ウェブブラウザの主流になっているGoogle Chromeや、Microsoft Edgeは、Blinkをベースとしている。ハードウェア側の性能やOSが新しくないとうまく挙動しないことがある。

### Gecko

ブラウザの[レンダリングエンジン](#レンダリングエンジン)の一種。Firefoxのもとになっているレンダリングエンジン。
