---
layout: default
main_title: サービス
sub_title:
type: service
---

クリアコードではフリーソフトウェアに基づいた開発、お客様のご要望に応じた既存ソフトウェアのカスタマイズ、導入サポートなどのサービスを行っております。
<h2 id='browserselector'>BrowserSelector</h2>

URLパターンに応じて起動するブラウザを切り替えるBrowserSelectorを提供します。
BrowserSelectorはInternet Explorer、Edge、Chrome、Firefoxといった主要なブラウザを幅広くサポートしています。

<ul class='link_list grid-x grid-padding-x'>
  <li class='cell medium-6 large-4'><a href='browserselector.html'>ブラウザ切替ツール BrowserSelector</a>
</ul>

<h2 id='fluentd'>Fluentd</h2>

クリアコードはFluentdおよびFluent Bitの開発、各種プラグインのメンテナンスを行っています。
Fluentd/Fluent Bitのサポート、導入支援、プラグイン開発を行います。

<ul class='link_list grid-x grid-padding-x'>
  <li class='cell medium-6 large-4'><a href='fluentd.html'>Fluentdサポートサービス</a>
  <li class='cell medium-6 large-4'><a href='fluent-bit.html'>Fluent Bit のご紹介</a>
</ul>

<h2 id='groonga'>Groonga</h2>

クリアコードはGroongaプロジェクトのメンバーとしてGroongaおよび関連プロダクトの開発を行っています。
Groonga/PGroonga/Mroongaの機能拡張、サポート、導入支援を行います。

<ul class='link_list grid-x grid-padding-x'>
  <li class='cell medium-6 large-4'><a href='groonga.html'>Groongaサポートサービス</a>
</ul>

<h2 id='data-processing-tool'>データ処理ツール開発</h2>

Apache Arrowを活用したデータ処理の効率化を支援します。

<ul class='link_list grid-x grid-padding-x'>
  <li class='cell medium-6 large-4'><a href='apache-arrow.html'>Apache Arrowのご紹介</a>
</ul>

<h2 id='browser-customize'>ブラウザカスタマイズ</h2>

クリアコードでは専用端末向けのブラウザカスタマイズや、組み込み機器向けのブラウザ開発などの、各種ブラウザに関するご相談を承っています。

<ul class='link_list grid-x grid-padding-x'>
  <li class='cell medium-6 large-4'><a href='browser-customize.html'>ブラウザカスタマイズ</a>
</ul>

<h2 id='mozilla'>Firefox/Thunderbird</h2>

FirefoxやThunderbirdの企業導入支援として、カスタマイズ、アドオン開発、サポートを提供します。

<ul class='link_list grid-x grid-padding-x'>
  <li class='cell medium-6 large-4'><a href='mozilla/menu.html'>Firefox/Thunderbirdサポートサービス</a>
</ul>


<h2 id='code-reader'>開発チームの支援</h2>

フリーソフトウェア開発の経験を活用して、開発チームを対象としたワークショップ/研修会を行っています。

<ul class='link_list grid-x grid-padding-x'>
  <li class='cell medium-6 large-4'><a href='code-reader'>コードリーダー育成支援</a>
</ul>

<h2 id='milter-manager'>メールシステム(milter manager)</h2>

迷惑メール対策システムの構築を簡単・低コストにする迷惑メール対策ソフトウェア「milter manager」のサポートやmilterの開発を行います。

<ul class='link_list grid-x grid-padding-x'>
  <li class='cell medium-6 large-4'><a href='milter-manager.html'>milter managerサービスメニュー一覧</a>
</ul>
