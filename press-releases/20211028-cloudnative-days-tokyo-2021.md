---
layout: default
main_title: CloudNative Days Tokyo 2021にクリアコードが登壇します
sub_title: 
type: press
---

<p class='press-release-subtitle-small'>
2021/11/4 18:20　 Fluentd/Fluent Bitで実現する楽なKubernetesのログ運用</p>

<div class="press-release-signature">
  <p class="date">2021年10月29日</p>
  <p>株式会社クリアコード
</div>


株式会社クリアコード（本社：埼玉県所沢市、代表取締役：須藤 功平）は、「+Native〜ともに繋げるクラウドネイティブの世界〜」をテーマとするカンファレンスCloudNative Days Tokyo 2021に登壇します。

クラウドネイティブは近年注目を集めているキーワードです。Kubernetesによるコンテナオーケストレーションが浸透する中で、ログ収集を担うFluentd/Fluent Bitに新たな側面から光が当たりつつあります。今回の講演では、Fluentdのメンテナーとして長年活動しているクリアコードが、クラウドネイティブ時代におけるFluentd/Fluent Bitの活用法について解説を行います。


##	講演の概要
* **日時**：2021年11月4日（金）18:20-18:40
* **講演タイトル**：Fluentd/Fluent Bitで実現する楽なKubernetesのログ運用
* **講演概要**：
  コンテナ運用で避けては通れないのがログ監視です。しかし、Kubernetesそのものは、ログの管理や収集に関する機能をほとんど提供していません。そこで登場するのが    Fluentd/Fluent Bitです。どちらもログを転送するためのデーモンプログラムで、CNCF傘下のプロジェクトとして開発されています。
  今回の講演では、Fluentd/Fluent Bitをどのように活用すればうまくログを収集できるのかを解説します。
* **講演者プロフィール**:  取締役・藤本誠二

  2012年からソフトウェアエンジニアとして、検索エンジンの開発やメンテナンスに従事。
  2019年よりFluentd/Fluent Bitのメンテナーとして活動。主な開発実績としてFluent BitのWindowsサポートの追加がある。

※　講演資料は、 当日以降に[ククログ](https://www.clear-code.com/blog/)にて公開予定です。　 


##	CloudNative Days Tokyo 2021の概要
* **開催日程**：2021年11月4日（木）～11月5日（金）
* **会場**：オンライン
* **参加費**：無料（事前登録制）
* **公式サイト**：https://event.cloudnativedays.jp/cndt2021


## クリアコードについて
クリアコードは、2006年7月にフリーソフトウェア開発者を中心に設立したソフトウェア開発会社です。

Fluentd/Fluent Bitに関して、本体ならびにプラグインを開発してきた実績からソースコードレベルのサポートを提供しています。これまでも、日本国内において、航空会社、官公庁、通信事業者をはじめとした大規模ユーザーに対して、インシデント時の原因究明から解決、本体へのフィードバックなどを含む様々な支援をしてきました。 ご相談にはFluentd/Fluent Bitのコアメンテナーが対応いたしますので、まずはお気軽にお問い合わせください。

### 参考URL
【コーポレートサイト】[{{ site.url }}{% link index.html %}]({% link index.html %})

【本プレスリリース】[{{ site.url }}{% link press-releases/20211028-cloudnative-days-tokyo-2021.md %}]({% link press-releases/20211028-cloudnative-days-tokyo-2021.md %})

【関連サービス】[{{ site.url }}{% link services/fluentd.md %}]({% link services/fluentd.md %})


### 当リリースに関するお問合せ先
株式会社クリアコード

TEL：04-2907-4726

メール：info@clear-code.com
