---
tags:
- fluentd
title: Fluentd pluginでconfig_paramとconfig_sectionを使いこなす
---
Fluentdのプラグインの開発をする上で避けて通れないのが設定です。ユーザーに設定ファイルを書いてもらってプラグインの動作をカスタマイズできるようにすることは必須です。
<!--more-->


`config_param`と`config_section`という2つのAPIを使いこなすとさまざまな設定を簡単にきれいに書くことができます。

### config_param

`config_param`は設定を定義するために使います。

次のように名前と型を指定するのが基本形です。

```ruby
desc "description for name"
config_param :name, :string
```


この場合は`:name`という名前の文字列型の設定を定義しています。型によって、使えるオプションが異なるので型ごとに説明します。なお、`config_param`の直前に`desc`を書くと設定の説明を書くことができます。説明を書いておくと[fluent-plugin-config-format]({% post_url 2017-03-09-index %})コマンドで説明付きで設定の定義をダンプすることができるようになります。

`type`として以下の9つの型を指定可能です。

  * `:string`

  * `:integer`

  * `:float`

  * `:size`

  * `:time`

  * `:bool`

  * `:enum`

  * `:array`

  * `:hash`

#### string, integer, float, bool

それぞれの型に変換して、指定された名前のインスタンス変数に値をセットします。

```ruby
config_param :name, :string, default: "", secret: true, alias: :full_name
```


  * `default`: デフォルト値を指定します。省略するとこの設定は必須になり、Fluentd起動時に設定されているかどうかチェックされます。

  * `secret`: 真を指定すると、Fluentd起動時のログなどで値がマスクされます。

  * `alias`: この設定の別名を指定します。

#### size

バイト単位のサイズを設定します。k,m,g,tというサフィックスを利用可能です。それぞれ、キロ、メガ、ギガ、テラを表します。大文字、小文字は区別しません。
サイズは以下のように整数に変換されます。

```text
limit 10  # 10 byte
limit 10k # 10240 byte
limit 10m # 10485760 byte
limit 10g # 10737418240 byte
limit 10t # 10995116277760 byte
```


使えるオプションはstring等と同じです。

#### time

時間の長さを指定します。s,m,h,dというサフィックスを利用可能です。それぞれ、秒、分、時、日を表します。小文字のみ有効です。
単位を省略すると`to_f`した値を使用します。秒に変換されます。

```text
interval 0.5 # 0.5秒
interval 1s  # 1秒
interval 1m  # 1分 = 60秒
interval 1h  # 1時間 = 3600秒
interval 1d  # 1日 = 86400秒
```


使えるオプションはstring等と同じです。

#### enum

列挙型です。ユーザーに特定の値のリスト内から設定値を選ばせたいときに使います。ユーザーがリスト内に存在しない値を設定した場合、Fluentd起動時にエラーが発生します。

```ruby
config_param :backend_library, :enum, list: [:geoip, :geoip2_c, :geoip2_compat], default: :geoip
```


  * `default`: デフォルト値を指定します。

#### array

配列です。

```ruby
config_param :users, :array, default: [], value_type: :string
```


このように書くと、以下のような設定で`@users = ["user1", "user2", "user3"]`と設定されます。

```text
users user1,user2,user3
```


  * `default`: デフォルト値を指定します。

  * `value_type`: 値の型を指定します。`:string, :integer, :float, :size, :bool, :time`のいずれかを指定できます。

#### hash

ハッシュです。設定ファイルの書き方は2通りあります。

```ruby
config_param :key_values, :hash, default: {}, symbolize_keys: true, value_type: :string
```


設定例:

```text
key_values {"key1": "value1", "key2": "value2"} # JSONで書く
key_values key1:value1,key2:value2
```


どちらも以下のようにパースされます。

```ruby
{ key1: "value1", key2: "value2" }
```


  * `default`: デフォルト値をハッシュリテラルで指定します。

  * `symbolize_keys`: 真を指定するとキーをシンボル化します。

  * `value_type`: 値の型を指定します。全ての値で同じ型を使用します。

### config_section

`config_section`は`config_param`をグループ化するために使用します。
具体例としては、組み込みのバッファーの設定、パーサーの設定などがあります。

例えば、fluent-plugin-s3では以下のように認証情報の設定、バッファーの設定、フォーマッターの設定をグループ化して記述することができます。fluent-plugin-s3では認証方法が複数あるので、認証方法ごとにセクションを作ることによって、利用している認証方法がわかりやすくなっています。このように設定をグループ化することによって設定ファイルの可読性が向上しています。

```aconf
<match *.log>
  @type s3
  <assume_role_credentials>
    role_arn xxxxx
    role_session_name xxxxx
  </assume_role_crecentials>
  <buffer tag,time>
    @type file
    path /var/log/fluent/s3
    timekey 3600 # 1 hour partition
    timekey_wait 10m
    timekey_use_utc true # use utc
  </buffer>
  <format>
    @type json
  </format>
</match>
```


`config_section` のメソッドシグニチャーは以下の通りです。

```ruby
config_section(name, root: false, param_name: nil, final: nil, init: nil, required: nil, multi: nil, alias: nil, &block)
```


  * `name`: セクション名をシンボルで指定します。デフォルトではここで指定した名前と同じ名前のインスタンス変数をプラグインインスタンス内で参照することができます。

  * キーワード引数

    * `root`: ルートセクションである場合に真を指定します。Fluentd内部で利用するもので、一般のプラグインでは利用しません。

    * `param_name`: プラグインのインスタンス内で利用する、インスタンス変数名を`@`を除いたシンボルまたは文字列で指定します。

    * `final`: プラグインのサブクラスでこのセクションの上書きを禁止するならば真を指定します。一般のプラグインでは、あまり意識しなくてもよいです。

    * `init`: このセクションでは初期値が必須であるパラメータのみを定義します。一般のプラグインでは、あまり意識しなくてもよいです。

    * `required`: このセクションが必須であれば真を指定します。真を指定した場合、設定ファイルにこのセクションがないとFluentdの起動時にエラーが発生します。

    * `multi`: このセクションを複数指定可能であれば真を指定します。

    * `alias`: このセクションの別名を指定します。

例: sample というプラグインに以下のようなセクションがある場合の設定ファイルの書き方を例示します

```ruby
config_section :child, param_name: 'children', required: false, multi: true, alias: 'node' do
  config_param :name, :string
  config_param :power, :integer, default: nil
  config_section :item, multi: true do
    config_param :name, :string
  end
end
```


```text
<source>
  @type sample
  <child>
    name  gohan
    power 100
    <item>
      name senzu
    </item>
  </child>
  <child>
    name  goten
    power 10
  </child>
</source>
```


### まとめ

`config_param`と`config_section`を使うことで得られる利点をまとめます。

  * :+1: 設定を宣言的に定義できる

  * :+1: `fluent-plugin-config-format`コマンドでMarkdownやJSONで定義を出力することができる

    * Markdownで出力したものはREADMEにそのまま貼り付けることも可能 :100:

  * :+1: コード量を少なくすることができる

`config_param`と`config_section`を使うことで、デメリットはありません。
Fluentd組み込みのAPIを使いこなして、使いやすくメンテナンスしやすいプラグインを書きましょう。
