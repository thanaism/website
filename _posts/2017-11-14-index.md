---
tags:
- mozilla
title: Thunderbirdの設定値の変更を監視するには
---
### はじめに

Thunderbirdのアドオンを作成する際に、設定値の変化を検出したい場合があります。
具体的には、次のようなケースです：
<!--more-->


  * ユーザーが関連する設定値を変更した時に、その効果を即座に反映させたい

  * アカウントの登録や変更のタイミングで、特定の処理をフックさせたい

MozillaのXPCOMライブラリには、いわゆる「オブザーバー」の仕組みが備わっています。
この仕組みを利用すれば、上記のような処理を比較的手軽に実装することができます。

以下の記事では、Thunderbirdの設定値の変更を検知して、任意の処理をフックする方法を解説いたします。

### 具体的な実装方法

[nsIPrefBranch](https://developer.mozilla.org/en-US/docs/Mozilla/Tech/XPCOM/Reference/Interface/nsIPrefBranch) インターフェイスに定義されている `addObserver()` メソッドを利用します。
例として、Thunderbirdの自動更新フラグ `app.update.auto` を監視するコードのサンプルを以下に示します：

```javascript
// ブランチオブジェクトを取得する
var Cc = Components.classes;
var Ci = Components.interfaces;
var prefs = Cc['@mozilla.org/preferences-service;1'].getService(Ci.nsIPrefBranch);

// 設定値にオブザーバーを登録する
prefs.addObserver('app.update.auto', function(aSubject, aTopic, aData) {
    // 設定が変更された時の処理
}, false);
```


このように定義すると、設定値 app.update.auto が変更されるたびに、二番目の引数で与えたオブザーバー関数が呼び出されるようになります（オブザーバー関数の引数については次節で説明します）。なお、最後の引数は「オブザーバーを弱参照で保持するか否か」を制御するブール値です。今回の例では単純に`false`（＝通常の参照を持つ）を指定しています [^0]

#### コールバック関数の引数について

登録したオブザーバー関数は、次の三つの引数を伴って呼び出されます：

|  引数名  |  内容                                 |
| -------- | ------------------------------------- |
| aSubject | 監視対象のブランチオブジェクト        |
| aTopic   | 文字列 "nsPref:changed"（固定値）     |
| aData    | 変更された設定名                      |

このうち`aSubject`と`aData`を組み合わせると、コールバック内で変更後の設定値を取得できます。以下に具体的な利用例を示します：

```javascript
prefs.addObserver('app.update.auto', function(aSubject, aTopic, aData) {
    aSubject.QueryInterface(Ci.nsIPrefBranch);
    var isAutoUpdate = aSubject.getBoolPref(aData);
    if (isAutoUpdate) {
        // Thunderbirdの自動更新がONの場合
    } else {
        // Thunderbirdの自動更新がOFFの場合
    }
}, false);
```


#### 複数の設定値をまとめて監視する

Thunderbirdの設定値は、一般に木構造をなしています。

実は `addObserver()` を使うと、末端の葉ノードだけではなく、中間にある内部ノードに対してオブザーバーを登録することもできます。この場合、対象のノードのすべての子孫ノードの変更について、登録したオブザーバー関数が呼び出されます。

例えば、`app.update`配下の設定値をまとめて監視したい場合は次のように記述します：

```javascript
prefs.addObserver('app.update', function(aSubject, aTopic, aData) {
    switch (aData) {
      case 'app.update.auto':
        ...
        break;
      case 'app.update.enabled':
        ...
        break
    }
}, false);
```


この記法は、自分のアドオンの設定値を一括して管理したい場合などに非常に有効です。

### まとめ

本記事では、Thunderbirdの設定値の変更を検知して、任意の処理をフックする方法を解説しました。

この仕組みを上手に使うと、設定値にまつわるイベントに対してリアクティブに反応できるようになるので、ユーザーの利便性を高めることができます。アドオンを作成される際は、ぜひお試しください。

[^0]: どのような場合にこのフラグを利用するかは https://developer.mozilla.org/en-US/docs/Mozilla/Tech/XPCOM/Weak_reference を参照ください。
