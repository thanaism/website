---
tags: []
title: 'Debianパッケージの作り方と公開方法: groongaを例にして'
---
注: Debianデベロッパーが書いた文章ではありません。Debianデベロッパーになりたい方はDebianが公式に配布している文書の方をお勧めします。
<!--more-->


Web上にはいくつかDebianパッケージの作り方を説明しているページがありますが、はじめてDebianパッケージを作る場合には情報不足のものが多いです。例えば、古めの文書で[CDBS](https://en.wikipedia.org/wiki/CDBS)を使っていなかったり、「あとは適当に修正して…」などと手順の一部が省略されている文書が多いです。

ここでは、全文検索エンジン兼カラムストアの[groonga](http://groonga.org/)を例にしてDebianパッケージの作り方を説明します。ここで説明するのは、1つのソースから1つのパッケージを作成するのではなく、1つのソースから複数のパッケージを作成する方法です。この方法は、ライブラリの場合に多く用いられます。

また、aptitudeでインストールできる形で公開する方法もざっくりと紹介します。ここで作成するDebianパッケージは以下のようにインストールすることができます。

<dl>






<dt>






Debian/GNU Linux lenny






</dt>






<dd>


以下の内容の/etc/apt/sources.list.d/groonga.listを作成


{% raw %}
```
deb http://packages.clear-code.com/debian/ lenny main
deb-src http://packages.clear-code.com/debian/ lenny main
```
{% endraw %}


インストール:


{% raw %}
```
% sudo apt-key adv --recv-keys --keyserver keyserver.ubuntu.com 1C837F31
% sudo aptitude update
% sudo aptitude -V -D -y install groonga libgroonga-dev
```
{% endraw %}


</dd>








<dt>






Debian/GNU Linux sid






</dt>






<dd>


以下の内容の/etc/apt/sources.list.d/groonga.listを作成


{% raw %}
```
deb http://packages.clear-code.com/debian/ unstable main
deb-src http://packages.clear-code.com/debian/ unstable main
```
{% endraw %}


インストール:


{% raw %}
```
% sudo apt-key adv --recv-keys --keyserver keyserver.ubuntu.com 1C837F31
% sudo aptitude update
% sudo aptitude -V -D -y install groonga libgroonga-dev
```
{% endraw %}


</dd>








<dt>






Ubuntu 8.04 LTS Hardy Heron






</dt>






<dd>


*注: Ubuntu本家のuniverseセクションもインストール対象としておくこと*


以下の内容の/etc/apt/sources.list.d/groonga.listを作成


{% raw %}
```
deb http://packages.clear-code.com/ubuntu/ hardy universe
deb-src http://packages.clear-code.com/ubuntu/ hardy universe
```
{% endraw %}


インストール:


{% raw %}
```
% sudo apt-key adv --recv-keys --keyserver keyserver.ubuntu.com 1C837F31
% sudo aptitude update
% sudo aptitude -V -D -y install groonga libgroonga-dev
```
{% endraw %}


</dd>








<dt>






Ubuntu 9.10 Karmic Koala






</dt>






<dd>


*注: Ubuntu本家のuniverseセクションもインストール対象としておくこと*


以下の内容の/etc/apt/sources.list.d/groonga.listを作成


{% raw %}
```
deb http://packages.clear-code.com/ubuntu/ karmic universe
deb-src http://packages.clear-code.com/ubuntu/ karmic universe
```
{% endraw %}


インストール:


{% raw %}
```
% sudo apt-key adv --recv-keys --keyserver keyserver.ubuntu.com 1C837F31
% sudo aptitude update
% sudo aptitude -V -D -y install groonga libgroonga-dev
```
{% endraw %}


</dd>


</dl>

それでは、まずはパッケージの作り方です。

### パッケージの作り方

dh_makeを利用する方法が紹介されていることが多いですが、dh_makeではたくさんのファイルが生成されるため、はじめてパッケージを作成する場合はとまどってしまいます。そのため、ここでは手動でパッケージを作成する方法を紹介します。

#### 構成

groongaはライブラリとgroongaのデータベースを管理するコマンドで構成されています。この場合、Debianでは以下のように複数のパッケージに分解します。

  * libXXX: ライブラリを使用しているソフトウェアを実行するために必要なファイルを提供するパッケージ。/usr/lib/libXXX.soなどを提供する。
  * libXXX-dev: ライブラリを使用しているソフトウェアをビルドするために必要なファイルを提供するパッケージ。/usr/include/XXX/*.hなどを提供することが多い。libXXXに依存する。
  * XXX: （ライブラリではなく）コマンドを提供するパッケージ。libXXXに依存する。

groongaの場合は以下のようなパッケージを作成することとします。

  * libgroonga: /usr/lib/libgroonga.soを提供。
  * libgroonga-dev: /usr/include/groonga/*.hと/usr/lib/pkgconfig/groonga.pcを提供。
  * groonga: /usr/bin/groongaを提供。

#### ソースのファイル名

Debianパッケージを作成する場合にはじめにつまずくポイントがファイルやディレクトリの名前付けの規則です。今回は現時点での最新リリースgroonga 0.1.5を利用するので、それを例にして説明します。

まず、元のソースをダウンロードし、展開します。

{% raw %}
```
% cd /tmp
% wget http://groonga.org/files/groonga/groonga-0.1.5.tar.gz
% tar xvzf groonga-0.1.5.tar.gz
groonga-0.1.5/
...
```
{% endraw %}

この状態ではDebianの名前付け規則から外れています。規則に合わせるためには以下のようにします。

{% raw %}
```
% mv groonga-0.1.5.tar.gz groonga_0.1.5.orig.tar.gz
```
{% endraw %}

つまり、以下のような構成にする必要があります。

{% raw %}
```
.
|--- groonga-0.1.5/
|    |...
|    ...
+--- groonga_0.1.5.orig.tar.gz
```
{% endraw %}

一般化するとこうです[^0]。

  * ソースのディレクトリ名: #&#123;パッケージ名&#125;-#&#123;バージョン&#125;
  * ソースのアーカイブ名: #&#123;パッケージ名&#125;_#&#123;バージョン&#125;.orig.tar.gz

#### debian/

ソースの準備ができたら、ソースを展開したディレクトリの直下にdebian/ディレクトリを作成します。Debianパッケージ用のファイルはこのディレクトリの下に置きます。

{% raw %}
```
% cd groonga-0.1.5
% mkdir debian/
```
{% endraw %}

このような構成になります。

{% raw %}
```
.
|--- groonga-0.1.5/
|    |--- debian/
|    |    |...
|    |...
|    ...
+--- groonga_0.1.5.orig.tar.gz
```
{% endraw %}

今回のパッケージ作成に必要なファイルは以下の通りです。それぞれ順番に説明します。

  * debian/control
  * debian/rules
  * debian/copyright
  * debian/changelog

#### debian/control

debian/controlにはパッケージ全体の情報と個々のパッケージの情報を書きます。

まず、パッケージ全体の情報です。

{% raw %}
```
Source: groonga
Priority: optional
Maintainer: Kouhei Sutou <kou@clear-code.com>
Build-Depends: debhelper (>= 5), cdbs, autotools-dev, libmecab-dev
Standards-Version: 3.7.3
Homepage: http://groonga.org/
```
{% endraw %}

それぞれ以下のような意味です。

<dl>






<dt>






Source






</dt>






<dd>


パッケージの元になるソースの名前


</dd>








<dt>






Priority






</dt>






<dd>


パッケージの優先度。自作パッケージはoptional。


</dd>








<dt>






Maintainer






</dt>






<dd>


パッケージメンテナー


</dd>








<dt>






Build-Depends






</dt>






<dd>


パッケージを生成するときに利用するパッケージ。debhelperとcdbsは必須。groongaは[Autotools](https://ja.wikipedia.org/wiki/Autotools)と[MeCab](https://ja.wikipedia.org/wiki/MeCab)を利用しているため、auttools-devとlibmecab-devも加えている。


</dd>








<dt>






Standards-Version






</dt>






<dd>


パッケージが準拠している[Debianポリシーマニュアル](http://www.debian.or.jp/community/devel/debian-policy-ja/policy.ja.html/)のバージョン。現在の最新バージョンは3.8.3。


3.7.3を指定しているのはUbuntu 8.04 LTS Hardy Heron用にもパッケージを作成するため。Hardyの時点ではバージョンが3.7.3だった。


</dd>








<dt>






Homepage






</dt>






<dd>


パッケージのWebサイト。ソースが入手できるようなサイト。


</dd>


</dl>

次に、個々のパッケージの情報を書きます。ここでは、libgroonga-devパッケージを例にして説明します。他のlibgroonga, groongaパッケージは後で示します。

{% raw %}
```
Package: libgroonga-dev
Section: libdevel
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}, libgroonga (= ${binary:Version})
Description: Development files to use groonga as a library
 Groonga is an open-source fulltext search engine and column store.
 It lets you write high-performance applications that requires fulltext search.
 .
 This package provides header files to use groonga as a library.
```
{% endraw %}

それぞれ以下のような意味です。

<dl>






<dt>






Package






</dt>






<dd>


パッケージ名


</dd>








<dt>






Section






</dt>






<dd>


パッケージが属するセクション。Debian公式のセクションは[Debianポリシーマニュアル - 2.4 セクション](http://www.debian.or.jp/community/devel/debian-policy-ja/policy.ja.html/ch-archive.html#s-subsections)にある。libgroongaは「lib」、libgroonga-devは「libdevel」、groongaは「database」に属することにする。


</dd>








<dt>






Architecture






</dt>






<dd>


ビルド可能なマシンアーキテクチャ。スクリプト言語のソースなどアーキテクチャに依存しない場合は「all」を指定する。どのアーキテクチャでもビルドできるときは「any」を指定する。


</dd>








<dt>






Depends






</dt>






<dd>


依存しているパッケージ。「$&#123;misc:Depends&#125;」、「$&#123;shlibs:Depends&#125;」はビルドされたファイルから自動的に検出された依存パッケージに置換される。/usr/bin/dh_*を見ると、他にも「$&#123;python:Depends&#125;」などがあるよう。


「libgroonga (= $&#123;binary:Version&#125;)」とバージョン指定付きで明示的にlibgroongaを指定しているのは、異なるバージョンのgroongaパッケージを使用しないようにするため。同じソースから生成したパッケージではバージョンまで指定しておく方が無難。


</dd>








<dt>






Description






</dt>






<dd>


パッケージの説明。1行目は短い説明を書き、2行目以降により詳細な説明を書く。2行目以降は先頭に1文字空白を入れることに注意。「.」だけの行は空行になる。


</dd>


</dl>

libgroonga, groongaパッケージも含んだdebian/controlは以下のようになります。それぞれのパッケージについての記述は空行で区切ります。

{% raw %}
```
Source: groonga
Priority: optional
Maintainer: Kouhei Sutou <kou@clear-code.com>
Build-Depends: debhelper (>= 5), cdbs, autotools-dev, libmecab-dev
Standards-Version: 3.7.3
Homepage: http://groonga.org/

Package: libgroonga-dev
Section: libdevel
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}, libgroonga (= ${binary:Version})
Description: Development files to use groonga as a library
 Groonga is an open-source fulltext search engine and column store.
 It lets you write high-performance applications that requires fulltext search.
 .
 This package provides header files to use groonga as a library.

Package: libgroonga
Section: libs
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Library files for groonga.
 Groonga is an open-source fulltext search engine and column store.
 It lets you write high-performance applications that requires fulltext search.
 .
 This package provides library files.

Package: groonga
Section: database
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}, libgroonga (= ${binary:Version})
Description: An open-source fulltext search engine and column store.
 It lets you write high-performance applications that requires fulltext search.
 .
 This package provides 'groonga' command.
```
{% endraw %}

#### debian/rules

debian/rulesにはパッケージの作り方を書きます。CDBS（Common Debian Build System）を使うとよくある処理をより簡潔に書くことができます。

groongaはAutotoolsを使っているのでCDBSのAutotools用のファイルを読み込みます。

{% raw %}
```makefile
#!/usr/bin/make -f

include /usr/share/cdbs/1/rules/debhelper.mk
include /usr/share/cdbs/1/class/autotools.mk
```
{% endraw %}

最初の行をみるとわかる通り、debian/rulesはMakefileと同じ書式を使います。

続いて、各パッケージで作成するディレクトリを指定します。変数の名前は「DEB_INSTALL_DIRS_#&#123;パッケージ名&#125;」となります。

{% raw %}
```makefile
DEB_INSTALL_DIRS_groonga =			\
        /usr/bin				\
        /usr/share/groonga

DEB_INSTALL_DIRS_libgroonga =			\
        /usr/lib

DEB_INSTALL_DIRS_libgroonga-dev =		\
        /usr/include/groonga			\
        /usr/lib/pkgconfig
```
{% endraw %}

groongaパッケージでは/usr/bin/groongaをインストールするので、/usr/binを指定する、libgroonga-devパッケージでは/usr/include/groonga/groonga.hをインストールするので/usr/include/groongaを指定するといった具合です。

最後にそれぞれのパッケージにどのファイルを含めるかを指定します。ターゲットの名前は「install/#&#123;パッケージ名&#125;」となります。ターゲットの後の「:」がふたつなのは実行する処理を追加しているためです。GNU makeの「::」の挙動について詳しく知りたい場合は[GNU makeのinfoのDouble-Colon Rulesのところ](http://www.gnu.org/software/make/manual/make.html#Double_002dColon)を読んでください。

{% raw %}
```makefile
install/groonga::
        cp -ar debian/tmp/usr/bin/* debian/groonga/usr/bin/
        cp -ar debian/tmp/usr/share/groonga/* \
          debian/groonga/usr/share/groonga/

install/libgroonga::
        cp -ar debian/tmp/usr/lib/libgroonga* debian/libgroonga/usr/lib/

install/libgroonga-dev::
        cp -ar debian/tmp/usr/include/groonga/* \
          debian/libgroonga-dev/usr/include/groonga/
        cp -ar debian/tmp/usr/lib/pkgconfig/* \
          debian/libgroonga-dev/usr/lib/pkgconfig/
```
{% endraw %}

ビルドしたファイルはdebian/tmp/以下にインストールされます。インストールされたファイルをcpでそれぞれのパッケージに振り分けています。通常はdh_movefilesを使うようなのですが`install/libgroonga`で使っているようなワイルドカードが使えないようなのでcpを使っています。

debian/rules全体はこうなります。

{% raw %}
```makefile
#!/usr/bin/make -f

include /usr/share/cdbs/1/rules/debhelper.mk
include /usr/share/cdbs/1/class/autotools.mk

DEB_INSTALL_DIRS_groonga =			\
        /usr/bin				\
        /usr/share/groonga

DEB_INSTALL_DIRS_libgroonga =			\
        /usr/lib

DEB_INSTALL_DIRS_libgroonga-dev =		\
        /usr/include/groonga			\
        /usr/lib/pkgconfig

install/groonga::
        cp -ar debian/tmp/usr/bin/* debian/groonga/usr/bin/
        cp -ar debian/tmp/usr/share/groonga/* \
          debian/groonga/usr/share/groonga/

install/libgroonga::
        cp -ar debian/tmp/usr/lib/libgroonga* debian/libgroonga/usr/lib/

install/libgroonga-dev::
        cp -ar debian/tmp/usr/include/groonga/* \
          debian/libgroonga-dev/usr/include/groonga/
        cp -ar debian/tmp/usr/lib/pkgconfig/* \
          debian/libgroonga-dev/usr/lib/pkgconfig/
```
{% endraw %}

#### debian/copyright

debian/copyrightにはソースの作者・ライセンスとパッケージの作者・ライセンス情報を書きます。ソースの作者・ライセンス情報はソースの中にある情報を利用します。groongaの場合はAUTHORSファイルに作者がリストされていました。著作権者の情報はソースのヘッダーファイルに、ライセンスの情報はCOPYINGにありました。

パッケージのライセンスはGPLv3+にしました。

{% raw %}
```
This package was debianized by Kouhei Sutou <kou@clear-code.com> on
Thu, 15 Jan 2010 14:52:04 +0000.

It was downloaded from <http://groonga.org/>

Upstream Author(s):

    Daijiro MORI <morita at razil. jp>
    Tasuku SUENAGA <a at razil. jp>
    Yutaro Shimamura <yu at razil. jp>
    Kouhei Sutou <kou at cozmixng. org>
    Kazuho Oku <kazuhooku at gmail. com>
    Moriyoshi Koizumi <moriyoshi at gmail. com>

Copyright:

    Copyright(C) 2009-2010 Brazil

License:

    This library is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 2.1 of the License.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

The Debian packaging is (C) 2010, Kouhei Sutou <kou@clear-code.com> and
is licensed under the GPL, see `/usr/share/common-licenses/GPL'.

# Please also look if there are files or directories which have a
# different copyright/license attached and list them here.
```
{% endraw %}

#### debian/changelog

debian/changelogには変更履歴を書きます。パッケージのバージョンはこのファイルから抽出されるのでパッケージのバージョンを上げた場合はこのファイルに追記することを忘れてはいけません。

debian/changelogに変更履歴を追記するための専用のコマンドdchがあるので、それを利用します。-vオプションで新しいバージョンを指定します。

{% raw %}
```
% dch -v 0.1.5
```
{% endraw %}

例えば、以下のように書きます。

{% raw %}
```
groonga (0.1.5) unstable; urgency=low

  * New upstream release

 -- Kouhei Sutou <kou@clear-code.com>  Mon, 18 Jan 2010 17:58:31 +0900
```
{% endraw %}

これでパッケージに必要な情報はできあがりました。

#### パッケージのビルド

パッケージをビルドするにはソースディレクトリで以下のコマンドを実行します。

{% raw %}
```
[groonga-0.1.5]% debuild -us -uc
```
{% endraw %}

-usは.dscファイルにGPGでサインしないオプションで、-ucは.changesファイルにGPGでサインしないオプションです。

パッケージ作成を試行錯誤しているときは毎回ソースをビルドするのは面倒なものです。そのときは以下のように-ncオプションを指定すると、ビルド前に`make clean`を実行しなくなるのでビルドしなおさなくなります。

{% raw %}
```
[groonga-0.1.5]% debuild -us -uc -nc
```
{% endraw %}

パッケージの作成が成功するとトップディレクトリにパッケージが作成されます。

{% raw %}
```
.
|--- groonga-0.1.5/
|    |--- debian/
|    |    |...
|    |...
|    ...
|--- groonga_0.1.5.orig.tar.gz
|--- groonga_0.1.5.diff.gz
|--- groonga_0.1.5.dsc
|--- groonga_0.1.5_amd64.build
|--- groonga_0.1.5_amd64.changes
|--- groonga_0.1.5_amd64.deb
|--- libgroonga-dev_0.1.5_amd64.deb
+--- libgroonga_0.1.5_amd64.deb
```
{% endraw %}

生成したパッケージはdpkgでインストールできます。

{% raw %}
```
[groonga-0.1.5]% sudo dpkg -i ../*groonga*.deb
```
{% endraw %}

### aptitude用に公開する方法

Debianパッケージを作るとインストールが楽になりますが、dpkgでインストールしている場合は依存関係を自動的に解決してくれません。依存関係を自動的に解決するためにはAPTリポジトリを作る必要があります。

ただし、用意するのはわりと面倒です。1つのDebian/Ubuntuのバージョン・アーキテクチャ向けであればまだ頑張ろうかという気にはなれますが、複数のバージョン・アーキテクチャをサポートする場合は嫌になることでしょう。

ここでは、[Cutter](/software/cutter.html)や[milter manager](/software/milter-manager.html)で使用しているDebianパッケージ作成からAPTリポジトリの更新を自動化しているスクリプトを紹介します。これらのスクリプトは[Cutterのリポジトリ](https://cutter.svn.sourceforge.net/svnroot/cutter/cutter/trunk/apt)などで公開されています。環境依存の部分があるので、利用する場合は自分の環境用に変更して利用してください。ライセンスはGPLv3+としますので、ライセンスの範囲内で自由に利用してください。

ただし、以下の説明はかなりざっくりとしているので気になる方は自分でスクリプトをみてください。

#### Debianパッケージ作成の自動化

複数のバージョン・アーキテクチャをサポートする場合、それぞれの環境ごとのDebianパッケージを作成することが大変です。ここで紹介するスクリプトではDebian GNU/Linux sid (x86_64)の環境にchroot環境を作って、すべて同じマシン上で作成します。複数のマシンで作成してもよいのですが、そうするとOSのインストールやsshの設定などが面倒になります。

pbuilderを使うことも多いようですが、その環境の中にシェルで入って確認したいこともあるので、常にchroot環境を持っていた方が便利です[^1]。

Cutterの場合はchroot環境内で以下のビルドスクリプトを実行しています。単純なコマンドの羅列なので説明は省略します。

{% raw %}
```sh
#!/bin/sh

VERSION=1.1.0

sudo aptitude -V -D update && sudo aptitude -V -D -y safe-upgrade
sudo aptitude install -y subversion devscripts debhelper cdbs autotools-dev \
    intltool gtk-doc-tools libgtk2.0-dev libgoffice-0-{6,8}-dev \
    libgstreamer0.10-dev libsoup2.4-dev

mkdir -p ~/work/c
if [ -d ~/work/c/cutter ]; then
    cd ~/work/c/cutter
    svn up
else
    cd ~/work/c
    svn co https://cutter.svn.sourceforge.net/svnroot/cutter/cutter/trunk cutter
fi

cd ~/work/c
rm -rf cutter-${VERSION}
tar xfz cutter_${VERSION}.orig.tar.gz
cd cutter-${VERSION}

mkdir debian
cp -rp ../cutter/debian/* debian/

if dpkg -l libgoffice-0-8-dev > /dev/null 2>&1; then
    :
else
    sed -i'' -e 's/libgoffice-0-8/libgoffice-0-6/g' debian/control
fi

debuild -us -uc
```
{% endraw %}

そして、このビルドスクリプトを各chroot環境内で実行するために以下のようなMakefileを使っています。Debian GNU/Linuxのlennyとsid、UbuntuのHardyとKarmicのパッケージを作成します。また、それぞれi386用とamd64用を生成するので8種類のパッケージを作成することになります。

{% raw %}
```makefile
VERSION = 1.1.0
SERVER_PATH = ktou,cutter@web.sourceforge.net:/home/groups/c/cu/cutter/htdocs
DISTRIBUTIONS = debian ubuntu
CHROOT_BASE = /var/lib/chroot
ARCHITECTURES = i386 amd64
CODES = lenny unstable hardy karmic

update:
        for code_name in $(CODES); do						\
          target=$${code_name}-$${architecture};				\
          case $${code_name} in							\
          lenny|unstable)							\
            distribution=debian;						\
            section=main;							\
            ;;									\
          *)									\
            distribution=ubuntu;						\
            section=main;							\
            ;;									\
          esac;									\
          (cd $${distribution};							\
           mkdir -p dists/$${code_name}/$${section}/source;			\
           for architecture in $(ARCHITECTURES); do				\
             mkdir -p dists/$${code_name}/$${section}/binary-$${architecture};	\
           done;								\
           apt-ftparchive generate generate-$${code_name}.conf;			\
           rm -f dists/$${code_name}/Release*;					\
           apt-ftparchive -c release-$${code_name}.conf				\
             release dists/$${code_name} > /tmp/Release;			\
           mv /tmp/Release dists/$${code_name};					\
           gpg --sign -ba -o dists/$${code_name}/Release{.gpg,};		\
          );									\
        done

upload: update
        for distribution in $(DISTRIBUTIONS); do		\
          (cd $${distribution};					\
           rsync -avz --exclude .svn --delete			\
             dists pool $(SERVER_PATH)/$${distribution};	\
          );							\
        done

download:
        for distribution in $(DISTRIBUTIONS); do			\
          (cd $${distribution};						\
           rsync -avz $(SERVER_PATH)/$${distribution}/pool/ pool;	\
          );								\
        done

build:
        for architecture in $(ARCHITECTURES); do				\
          for code_name in $(CODES); do						\
            target=$${code_name}-$${architecture};				\
            case $${code_name} in						\
            lenny|unstable)							\
              distribution=debian;						\
              section=main;							\
              ;;								\
            *)									\
              distribution=ubuntu;						\
              section=main;							\
              ;;								\
            esac;								\
            build_dir=$(CHROOT_BASE)/$$target/home/$$USER/work/c;		\
            pool_dir=$$distribution/pool/$$code_name/$$section/c/cutter;	\
            mkdir -p $$build_dir;						\
            cp ../cutter-$(VERSION).tar.gz					\
              $$build_dir/cutter_$(VERSION).orig.tar.gz &&			\
            sudo su -c								\
              "chroot $(CHROOT_BASE)/$$target su - $$USER" < build-deb.sh;	\
            mkdir -p $$pool_dir;						\
            cp -p $$build_dir/*cutter*_$(VERSION)* $$pool_dir;			\
          done;									\
        done
```
{% endraw %}

細かい内容は省略しますが、以下のように使用します。準備が大変です。

{% raw %}
```
% sudo mkdir -p /var/lib/chroot/
% sudo su -c 'debootstrap --arch i386 lenny /var/lib/chroot/lenny-i386'
% sudo su -c 'debootstrap --arch amd64 lenny /var/lib/chroot/lenny-amd64'
% sudo su -c 'debootstrap --arch i386 sid /var/lib/chroot/sid-i386'
% sudo su -c 'debootstrap --arch amd64 sid /var/lib/chroot/sid-amd64'
% sudo su -c 'debootstrap --arch i386 hardy /var/lib/chroot/hardy-i386'
% sudo su -c 'debootstrap --arch amd64 hardy /var/lib/chroot/hardy-amd64'
% sudo su -c 'debootstrap --arch i386 karmic /var/lib/chroot/karmic-i386'
% sudo su -c 'debootstrap --arch amd64 karmic /var/lib/chroot/karmic-amd64'
% sudo vim /etc/fstab
proc   /var/lib/chroot/lenny-i386/proc    proc   defaults 0 0
devpts /var/lib/chroot/lenny-i386/dev/pts devpts defaults 0 0
sysfs  /var/lib/chroot/lenny-i386/sys     sysfs  defaults 0 0
...
% sudo mount -a
...それぞれのchroot環境に入って作業用ユーザを作成し、sudo可能にする...
```
{% endraw %}

準備ができたら以下のコマンドでパッケージがビルドできます。

{% raw %}
```
% cd /tmp
% svn export https://cutter.svn.sourceforge.net/svnroot/cutter/cutter/trunk/apt
% wget http://downloads.sourceforge.net/cutter/cutter-1.1.0.tar.gz
% cd apt
% make build
```
{% endraw %}

成功すると*/pool/*/main/c/cutter/以下に.debが生成されています。

{% raw %}
```
% ls */pool/*/main/c/cutter/*.deb
debian/pool/lenny/main/c/cutter/cutter-bin_1.1.0-1_i386.deb
debian/pool/lenny/main/c/cutter/cutter-doc_1.1.0-1_i386.deb
...
```
{% endraw %}

この状態でAPTリポジトリを作成できます。

{% raw %}
```
% make update
```
{% endraw %}

GPGでサインしようとするのであらかじめGPGの設定を行っておいてください。APTリポジトリの作成に関する設定はdebian/*.confとubuntu/*.confです。環境に合わせて変更してください。

Makefileの中にはuploadというターゲットがあります。このターゲットを使うとupdateで作成したAPTリポジトリをsf.netのWebサーバにrsyncでアップロードできます。

### まとめ

groongaを例にしてDebianパッケージの作成方法を説明しました。ついでに、APTリポジトリの公開方法もざっくりと紹介しました。

Debianパッケージの作り方やAPTリポジトリの公開方法は、必要な人はあまりいないかもしれませんが、groongaのDebianパッケージを必要としている人はそれよりもいそうな気がします。もしよかったら使ってみてください。

[^0]: tar.gzではなくtar.bz2などでもOKです。

[^1]: そもそもpbuilderは複数の環境を切り替えてビルドすることができるのでしょうか。環境を作り直さなければいけない気がします。
