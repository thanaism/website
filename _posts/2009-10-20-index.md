---
tags:
- mozilla
title: CPUの使用率を表示するFirefoxアドオン「システムモニター」を更新しました
---
CPUの使用率をFirefoxのツールバー上に表示するアドオン「システムモニター」のバージョン0.2をリリースしました。以下のリンク先からダウンロードできます。
<!--more-->


  * [packages.clear-code.comからダウンロード](http://packages.clear-code.com/xul/extensions/system-monitor/system-monitor-0.2.xpi)
  * [addons.mozilla.orgからダウンロード](https://addons.mozilla.org/firefox/addon/15093)

[前のバージョン]({% post_url 2009-10-19-index %})からの変更点は以下の通りです。

  * グラフの左右端をドラッグすることで任意の幅に変更できるようになりました。
  * ツールバーのカスタマイズでグラフを任意の位置に移動できるようになりました。（初回起動時に、ツールバーに項目を追加するかどうかの確認を求めるようになりました。）

### 導入手順

インストールすると、初回起動時に以下のイメージのようなダイアログが表示されます。

![初回起動時の確認ダイアログ]({{ "/images/blog/20091020_0.png" | relative_url }} "初回起動時の確認ダイアログ")

「はい」を選択すると、メニューバーの右端（Mac OS Xではナビゲーションツールバーの右端）に、以下のイメージのようにCPU使用率のグラフが表示されます。

![CPUモニター]({{ "/images/blog/20091020_1.png" | relative_url }} "CPUモニター")

プラットフォームは、WindowsXP SP1以降、Mac OS X、Linuxをサポートしています。Linuxではlibgtop2を使用していますので、別途インストールしてください。

### Web APIの利用

このアドオンを導入した環境では、システムモニターが提供するAPIを通じて、Webページ上のスクリプトからシステムモニターと同じ情報を取得できるようになります。以下は、Webページ内にCPU使用率のグラフを埋め込む例です。実際に上記リンク先からアドオンをインストールした状態で、このページを表示してみて下さい。

{% raw %}
```html
<div id="system-monitor-demo"></div>
<script type="text/javascript"><!--
var container = document.getElementById("system-monitor-demo");
if (!window.system || !window.system.addMonitor) {
  container.innerHTML = "システムモニターがインストールされていません";
  container.style.color = "red";
} else {
  container.innerHTML = "<div><canvas id='system-monitor' width='300' height='60'></canvas>"+
                        "<br /><span id='system-monitor-console'></span></div>";

  var width = 300;
  var interval = 1000;

  var CPUTimeArray = [];
  var arrayLength = width / 2;
  while (CPUTimeArray.length < arrayLength) {
    CPUTimeArray.push(undefined);
  }

  // リスナとして登録する関数には、CPUの使用率が割合として渡される。
  function onMonitor(aUsage) {
    var console = document.getElementById("system-monitor-console");
    console.textContent = aUsage;

    CPUTimeArray.shift();
    CPUTimeArray.push(aUsage);

    var canvasElement = document.getElementById("system-monitor");
    var context = canvasElement.getContext("2d")
    var y = canvasElement.height;
    var x = 0;

    context.fillStyle = "black";
    context.fillRect(0, 0, canvasElement.width, canvasElement.height);

    context.save();
    CPUTimeArray.forEach(function(aUsage) {
      var y_from = canvasElement.height;
      var y_to = y_from;
      if (aUsage == undefined) {
        drawLine(context, "black", x, y_from, 0);
      } else {
        y_to = y_to - (y * aUsage);
        y_from = drawLine(context, "green", x, y_from, y_to);
        drawLine(context, "black", x, y_from, y_to);
      }
      x = x + 2;
    }, this);
    context.restore();
  }

  function drawLine(aContext, aColor, aX, aBeginY, aEndY) {
    aContext.beginPath();
    aContext.strokeStyle = aColor;
    aContext.lineWidth = 1.0;
    aContext.lineCap = "square";
    aContext.moveTo(aX, aBeginY);
    aContext.lineTo(aX, aEndY);
    aContext.closePath();
    aContext.stroke();
    return aEndY - 1;
  }

  // リスナを登録する。
  window.system.addMonitor("cpu-usage", onMonitor, interval);

  window.addEventListener("unload", function() {
    window.removeEventListener("unload", arguments.callee, false);
    // ページのアンロード時にリスナの登録を解除する必要がある。
    window.system.removeMonitor("cpu-usage", onMonitor);
  }, false);
}
// --></script>
```
{% endraw %}

### 次世代のWebアプリケーションに向けて

上の例をご覧いただいても分かるとおり、このアドオンは、Webアプリケーション（Webページ上のスクリプト）から各種ハードウェアデバイスへアクセスする技術のデモンストレーションとして開発されています。

Webカメラとの連動によるAR（拡張現実）や、指紋認証、Felica認証といった特殊なハードウェアを必要とする認証技術など、この技術を応用することにより今までのWebアプリケーションではできなかった様々なことが可能となります。

クリアコードでは、このようにWebとハードウェアを直結する技術の開発に取り組んでおります。特殊なデバイスを搭載したハードウェアへのWebブラウザエンジンの組み込みや、特殊なデバイスの利用を前提としたWebベースのシステム開発など、ご用命がございましたらぜひinfo@clear-code.comまでお問い合わせください。
