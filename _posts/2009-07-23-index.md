---
tags:
- test
title: Pikzie 0.9.5リリース
---
Python用の書きやすくデバッグしやすい単体テスティングフレームワーク[Pikzie](http://pikzie.sourceforge.net/index.html.ja) 0.9.5をリリースしました。
<!--more-->


easy_installでもpipでもインストールできます。

{% raw %}
```
% sudo easy_install Pikzie
```
{% endraw %}

または

{% raw %}
```
% sudo pip install Pikzie
```
{% endraw %}

今回は、Windowsでもある程度動くようになったのでリリースしました。

新機能はテストを省略する[omit](http://pikzie.sourceforge.net/assertions.html.ja#Assertions-omit)の追加です。
