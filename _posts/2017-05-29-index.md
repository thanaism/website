---
tags:
- ruby
- company
title: '関西Ruby会議2017：株式会社クリアコード #kanrk2017'
---
須藤です。今年2回目の大阪です。（今年1回目の大阪は[OSS Gate大阪ワークショップ2017-02-25]({% post_url 2017-03-01-index %})でした。）
<!--more-->


2017年5月27日に[関西Ruby会議2017](http://regional.rubykaigi.org/kansai2017/)が開催されました。「Ruby Community and Ruby Business」がテーマということだったので、会社の話をするのがいいだろうなぁと思い、「株式会社クリアコード」というタイトルで基調講演をしました。

<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/kansai-rubykaigi-2017/viewer.html"
          width="640" height="524"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/kansai-rubykaigi-2017/" title="株式会社クリアコード">株式会社クリアコード</a>
  </div>
</div>


関連リンク：

  * [スライド（Rabbit Slide Show）](https://slide.rabbit-shocker.org/authors/kou/kansai-rubykaigi-2017/)

  * [スライド（SlideShare）](https://www.slideshare.net/kou/kansairubykaigi2017)

  * [リポジトリー](https://github.com/kou/rabbit-slide-kou-kansai-rubykaigi-2017)

### 内容

「Ruby Community and Ruby Business」というテーマなので、クリアコードは「Ruby Community」と「Ruby Business」を相互に活かしながらクリアコードが大事にしていることを実現している、という話をしました。クリアコードでの「Ruby Community」は「Ruby関連のフリーソフトウェア開発活動」、「Ruby Business」は「クリアコードでの仕事の仕方・作り方」です。クリアコードが大事にしていることは「フリーソフトウェアの推進と稼ぐことの両立」です。

1つのストーリーになった話ではなく、関連する話をたくさん集めた話にしました。具体的には次の話をしました。

  * 問題はupstreamで直す

  * 開発を続けられるコードを書く

  * 相手が想像しなくてもわかるように説明する

  * 楽しく開発する

  * 非難するよりも手を動かす

  * 回避策よりも根本解決

  * 受託開発

  * FLOSSサポート

  * OSS開発支援

  * 仕事の作り方：お客さん探しを頑張らない

  * Apache Arrow

  * 採用

最初の方の話は[開発スタイル](/philosophy/development/style.html)にまとまっている話です。

テーマに沿った内容になったと思っているのですが、いかがだったでしょうか？

### まとめ

関西Ruby会議2017で基調講演をしてきました。「株式会社クリアコード」というタイトルでフリーソフトウェア開発と会社の話をしました。

この話を読んで仕事が決まる・採用の応募があるとすごく話がキレイにまとまるので、関西Ruby会議2017ではタイミングを逃してしまって話しかけられなかったという人は、遠慮せずにまだ間に合うのでご連絡ください。
