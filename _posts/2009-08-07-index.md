---
tags:
- ruby
title: もうすぐRetrospectiva 2.0がリリース
---
Rubyで実装されたプロジェクト管理システムである[Retrospectiva](http://retrospectiva.org/)のバージョン2.0がまもなくリリースされます。[cozmixng.orgで最新バージョンが運用されている](http://www.cozmixng.org/retro/)ので、それを触ってみることで最新の機能を確認することができます。見てもらえばわかる通り、日本語表示にも対応しています。
<!--more-->


Retrospectivaは一時期開発が停滞していて、その間にRedmineの方が普及しました。しかし、その後、再び開発が活発になり、現在は[2.0 RC1](http://retrospectiva.org/blog/27)がリリースされています。1.xから2.0では多くの改良が行われています。そのいくつかを紹介します。

### 簡単インストール

[Single Step Installer](http://retrospectiva.org/wiki/Single-Step-Installer)が用意されていて、コマンド一発でインストールできるようになっています。以前より導入の敷居が下がっています。

### アジャイル開発支援

[AgilePM](http://retrospectiva.org/wiki/AgilePM)というアジャイル開発を支援するプラグインが公開され、プロジェクト管理機能がさらに充実しています。[tDiaryプロジェクト用のAgilePM](http://www.cozmixng.org/retro/projects/tdiary/stories)があるので、そこで触ってみることができます。ただし、現時点ではまだ利用されていないのであまり雰囲気がわからないかもしれません。これからのtDiaryプロジェクトの利用に期待しましょう。

### git対応

Subversionだけではなく、gitにも対応しました。また、Retrospectiva自体のバージョン管理システムもSubversionからgitに移行しています。

最近はgitを採用するプロジェクトも増えているため、これは嬉しい機能ではないでしょうか。

### まとめ

まもなくリリースされるRetrospectiva 2.0を簡単に紹介しました。以前は「ブログがついたTrac」みたいな書かれ方をされていたRetrospectivaですが、実際に使ってみるとその表現が間違っていたことに気付いた人も多かったのではないでしょうか。以前から[コミットログで連携]({% post_url 2008-05-23-index %})する機能などがあり、使っていた人は「便利なプロジェクト管理ツール」という方がしっくりくることに気付いていたはずです。2.0ではより便利で有用な機能がスマートなインターフェイスで追加されています。2.0の紹介のために「ブログがついた〜」と書かれることは減ることでしょう。

Redmineもよいですが、プロジェクト管理ツールとしてRetrospectivaも検討してみてはいかがでしょうか。

もし、使用してみてRetrospectivaの開発に参加したくなった場合は[Retrospectivaを使って開発に参加](http://retrospectiva.org/tickets/new)するとよいでしょう。まずは、[未翻訳メッセージの翻訳](http://retrospectiva.org/wiki/Translations)から参加するのが敷居が低いでしょう。kou@clear-code.comまで連絡してもらえれば相談にのります。
