---
tags: []
title: LXCコンテナにホスト側のユーザをバインドする方法
---
LXCコンテナを作ったとき、毎回自分の設定をコピーするのは面倒です。
dotfilesを変更する度にLXCコンテナ側に設定を反映するのは特に面倒です。
<!--more-->


Ubuntuコンテナを作るときは`--bindhome`オプションを付けることで自動的にユーザを追加することができます。
しかし、他のディストリビューション向けのテンプレートにはそのような機能はありません。
そこで、手動でホスト環境にいる自分ユーザをコンテナ側に追加してホームディレクトリを共有する手順を紹介します[^0]。

ここではFedora18のコンテナを使って説明します[^1]。

まず、コンテナを作成します。

{% raw %}
```
$ sudo lxc-create -t fedora -n f18 -- --release 18 --arch x86_64
```
{% endraw %}

次にコンテナに追加したいユーザのuid/gidを調べます。

{% raw %}
```
$ id -u
$ id -g
```
{% endraw %}

この2つの値を覚えておきます。ここでは`uid=1000,gid=1000`とします。
また、ユーザ名とグループ名は`fedora`とします。

コンテナにグループを追加してから、ユーザを追加します。

{% raw %}
```
$ sudo chroot /var/lib/lxc/f18/rootfs groupadd --gid=1000 fedora
$ sudo chroot /var/lib/lxc/f18/rootfs useradd --uid=1000 --gid=fedora --create-home -s /bin/bash fedora
```
{% endraw %}

パスワードを設定します。ここではパスワードも`fedora`にしてあります[^2]。

{% raw %}
```
$ echo "fedora:fedora" | sudo chroot /var/lib/lxc/f18/rootfs chpasswd
```
{% endraw %}

ホームディレクトリを共有するための設定をコンテナに追加します。

{% raw %}
```
$ sudo sh -c "echo '/home/fedora home/fedora none bind 0 0' >> /var/lib/lxc/f18/fstab"
```
{% endraw %}

追加したいユーザの分だけ、この手順を繰り返します。
これでコンテナを起動すると、追加したユーザが使えるようになっています。

### まとめ

LXCコンテナにホスト側のユーザをバインドする方法を紹介しました。

[^0]: Ubuntuテンプレートでやっていることを手動でやります

[^1]: LXCのバージョンは1.0を想定しています

[^2]: /etc/shadowのエントリをコピーすればパスワードもホスト環境と同じものを使うことができます。
