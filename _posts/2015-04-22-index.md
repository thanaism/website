---
tags:
- clear-code
title: 6月24日開催 アジャイルアカデミー「実践リーダブルコード」募集開始のお知らせ
---
6/24(水)にアジャイルアカデミー[「実践リーダブルコード」](http://event.shoeisha.jp/aa/20150624)を開催することになりました。[前回の3月6日]({% post_url 2015-03-09-index %})に続いて、2回目の開催です。
<!--more-->


### 「コードの読み手の視点」を身につける

「実践リーダブルコード」は、「リーダブルコードを書けるようになりたい人」や「リーダブルコードを書く開発チームにしたい人」が対象です。

ただし、このワークショップではリーダブルコードを書くためのテクニックは学びません。リーダブルコードを書くために必要な「コードの読み手の視点」を身につける事が、この講座の目標です。

リーダブルなコードかどうかはコードを読む側が判断するものですので、リーダブルなコードを書くためには、この「コードの読み手の視点」が不可欠です。この講座では「読む人が 読みやすいなら リーダブル」を合い言葉にして、今までとは違った角度からリーダブルコードへの理解を深めていきます。

### 「1年目から身につけたい！ チーム開発6つの心得」にも関連

クリアコードでは、4/24発売[WEB+DB PRESS Vol.86の特集記事「1年目から身につけたい！ チーム開発6つの心得」を執筆しました。]({% post_url 2015-04-08-index %})この記事は、チームによる開発をうまく回していくために不可欠な、「良いコードを書くこと」と「良いコミュニケーション」を実現するためのノウハウを紹介しています。

「実践リーダブルコード」で伝えたいことは、この記事に通じる内容です。「実践リーダブルコード」でも、「チーム開発6つ心得」で紹介するノウハウのいくつかを学ぶことができます。チーム開発6つの心得に取り組んでみようという方も、是非「実践リーダブルコード」の受講をご検討ください。

### 新人、ベテラン問わず学べる

実践リーダブルコードでは、開発チームでリーダブルコードを実践する方法として、「チームの『よい』を共有して、それを育てる」というやり方を学びます。これは「コーディング規約を作って守らせる」や「コードレビューで問題を指摘する」といったアプローチとは大きく異なりますので、開発経験豊富なベテランの方にとっても新鮮な学びになることでしょう。実際に、前回の受講者からもそのような声を多くいただきました。

### まとめ

6/24に開催するアジャイルアカデミー「実践リーダブルコード」の募集を開始しましたので、講座の特徴を紹介しました。前回の内容は[GitHubで公開](https://github.com/clear-code/readable-code-workshop)していますので、ご検討の際は参考としてください。

ご興味をお持ちになられた方は、是非[アジャイルアカデミーのページ](http://event.shoeisha.jp/aa/20150624/)からお申し込みください。
