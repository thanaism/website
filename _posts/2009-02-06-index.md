---
tags:
- milter-manager
title: milter manager 0.8.0
---
迷惑メール対策ソフトウェア、[milter manager](http://milter-manager.sourceforge.net/index.html.ja) 0.8.0をリリースしました。
<!--more-->


  * [リリースアナウンス](https://sourceforge.net/mailarchive/forum.php?thread_name=20090206.093340.57809163873952839.kou%40clear-code.com&forum_name=milter-manager-users-ja)
  * [変更点](http://milter-manager.sourceforge.net/reference/ja/news.html)

milter managerは[IPA](http://www.ipa.go.jp/)の[２００８年度 オープンソフトウェア利用促進事業 上期テーマ型（開発） 公募](http://www.ipa.go.jp/software/open/ossc/2008/theme/koubo1.html)「迷惑メール対策ポリシーを柔軟に実現するためのmilterの開発」で開発しています。

2月21日（土）には[オープンソースカンファレンス2009 Tokyo/Spring](http://www.ospn.jp/osc2009-spring/)の[ライトニングトーク](http://www.ospn.jp/osc2009-spring/modules/eguide/event.php?eid=21)でmilter managerの紹介をします。milter managerを導入した場合のシステム全体の紹介よりも、Rubyインタプリタを組み込んだアプリケーションとしての側面に重みをおいて紹介する予定です。

Ruby関連のイベントでは、同日に[とちぎRuby会議01](http://regional.rubykaigi.org/tochigi01)がありますが、栃木まで行くのは大変、という方はオープンソースカンファレンスに参加してみてはいかがでしょうか。
[日本Rubyの会](http://jp.rubyist.net/)も展示をしたり、[セッションを開いたり](http://www.ospn.jp/osc2009-spring/modules/eguide/event.php?eid=53)するようです。[クリアコードの展示]({% post_url 2009-02-02-index %})も面白そうです。

[オープンソースカンファレンス2009 Tokyo/Spring](http://www.ospn.jp/osc2009-spring/)は2月20日（金）、2月21日（土）の2日間開催されます。予定が空いている方は参加してみてはいかがでしょうか。最近の技術の動向を知るよい機会だと思います。
