---
tags:
- ruby
title: '名札には名前を大きく書きましょうジェネレータ改: cairoとPangoでPDF生成'
---
注: この記事にライセンスは設定されていません。
<!--more-->


早いもので来週末は[日本Ruby会議2010](http://rubykaigi.org/2010/ja)です。日本Ruby会議では、たくさんいる（会ったことはないけど名前を知っている）参加者がお互いを認識しやすいように大きな名札をつけることが恒例となっています。

RubyKaigi日記でも[名札には名前を大きく書きましょう](http://rubykaigi.tdiary.net/20100815.html#p01)と呼びかけています。この中で、「あらかじめ太くて大きなフォントで、黒々と印刷してきたものを持参して、名札に貼り付けるのはいかがでしょう。」と提案しています。しかし、自分でデザインするのはわりと面倒なものです。

そこで、kdmsnrさんが[名札には名前を大きく書きましょうジェネレータ](http://capsctrl.que.jp/kdmsnr/diary/20100815.html#p01)を作りました。これはtwitter IDを指定するだけでRubyKaigi日記で提案されているようなデザインの画像を生成してくれます。

[![@kdmsnr]({{ "/images/blog/20100818_0.jpg" | relative_url }} "@kdmsnr")](http://rubykaigi.capsctrl.com/kdmsnr)

でも、印刷するならPDFの方が嬉しいよね、ということでPDFを出力できるように改造したのが[名札には名前を大きく書きましょうジェネレータ改](http://nameplate.clear-code.com/)です。

[![@kdmsnr改]({{ "/images/blog/20100818_1.png" | relative_url }} "@kdmsnr改")](http://nameplate.clear-code.com/kdmsnr)

PDFも出力できるようにした他に、フォントを選べたり、細かく調整するためにSVGも出力できるようにしています。それでは、どのように実現しているかを説明します。

### 使っているもの

描画には[cairo](https://ja.wikipedia.org/wiki/cairo)、文字の配置には[Pango](https://ja.wikipedia.org/wiki/Pango)、画像の読み込みにはGdkPixbufを用いています。どれもLinux、*BSD、Mac OS X、Windowsなど多くの環境で動作するライブラリです。ここでは、cairoとPangoだけ説明します。

### cairo

cairoは2次元グラフィックを生成するためのライブラリで以下のような特長があります。

  * ベクトルベースのAPI
  * 描画処理のコードを変更せずに出力先を変えることができる

ベクトルベースのAPIとなっているということは品質を落とさずに拡大・縮小ができるということです。ジェネレータ改では、実際のサイズの画像とサムネイル画像を生成しますが、このようなことが以下のように描画処理を変更せずに実現できます。

{% raw %}
```ruby
def render(context)
  # 実際のサイズの描画
end

# 実際のサイズを描画するとき
render(context)

# 1/3サイズのサムネイルを描画するとき
context.scale(1 / 0.3, 1 / 0.3) # 描画処理の前にこれを呼ぶだけでOK
render(context)
```
{% endraw %}

描画処理のコードを変更せずに出力先を変えることができると、描画結果はPNGにしてブラウザで確認、印刷するときはPDF、編集する時はSVG、というように用途にあわせたグラフィックのフォーマットを提供することが簡単にできるということです。これは今回のようなWeb上で印刷物を生成する場合はとても便利な機能です。いちいちPDFで確認するのは面倒ですよね。サムネイルで一覧表示する場合もPNG+ブラウザの方が便利です。

cairoの詳しい使い方は[Rubyist Magazine - cairo: 2 次元画像描画ライブラリ](http://jp.rubyist.net/magazine/?0019-cairo)を見てください。

### Pango

Pangoは多言語に対応したテキストの配置を行うライブラリです。フォントの扱いなどテキストの配置に関することを抽象化してくれるので、TTFやOTFなどフォントフォーマットの違いや、フォントファイルをどこに置くかなどをプログラム側で気にする必要がありません。

ジェネレータ改ではインストールされているフォントを列挙したり、できるだけ大きいテキストサイズを自動検出するためにPangoを利用しています。テキストを中央揃えにするのもPangoの機能を利用しています。

システムにインストールされているフォントの一覧は以下のように取得できます。

{% raw %}
```ruby
font_families = Pango::CairoFontMap.default.families.collect do |family|
  family.name
end
p font_families # => ["Mona", "梅明朝S3", "衡山毛筆フォント草書", ...]
```
{% endraw %}

残念ながらPangoを利用するためのまとまった日本語の資料はありません。興味のある人はソースコードを見てください。

### まとめ

[名札には名前を大きく書きましょうジェネレータ改](http://nameplate.clear-code.com/)をネタにしてcairoとPangoを紹介してみました。cairoとPangoはFirefoxやGTK+などデスクトップで使われることが多いライブラリですが、Webアプリケーションのようにサーバサイドでも有用なライブラリです。ジェネレータ改のように用途にあわせて画像のフォーマットを使い分けたい場合は、cairoとPangoを使ってみてはいかがでしょうか。また、日本Ruby会議2010に参加する人はジェネレータで作った名札を印刷して持っていってはいかがでしょうか。

[名札には名前を大きく書きましょうジェネレータ改のソースコード](http://github.com/kou/nafuda_generator/)はGitHubにあります。
