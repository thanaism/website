---
tags:
- feedback
title: Debianで医用画像を閲覧するためのアプリケーション三選
---
### はじめに

最近骨折を経験した林です。
病院を紹介してもらうときに、紹介状とともに医用画像を含むメディア（CD-ROM）を渡されることがあります。
メディアには医用画像を閲覧するための専用のビューワーが付属していたりしますが、そのビューワーがWindowsでのみ動作するアプリケーションだったりするとそのままではDebianで閲覧できません。
そこで、今回はDebianでこのような医用画像（いわゆるDICOMと呼ばれるフォーマットのもの）を閲覧する方法をいくつか紹介します。
<!--more-->


閲覧時のサンプルの画像には[日本画像医療システム工業会:DICOMの世界](http://www.jira-net.or.jp/dicom/dicom_data_01_02.html)を利用しました。

### aeskulap

[Aeskulap - DICOM Viewer](http://aeskulap.nongnu.org/)

2005年から開発されているDICOMビューワーです。
公式には2007年の0.2.1が最新ですが、Debianではベータ版である0.2.2-beta2がパッケージ化されています。
使ってみたことはないのですが、ネットワークを経由してPACSとよばれるノードからDICOM画像を検索する機能も備えているようです。

画像によっては埋め込まれている患者名の扱いによって落ちる不具合があります。この点についてはすでにアップストリームにバグ報告しています。

[[Aeskulap-users] Stack smashing bug when invalid patient name parsing](http://lists.nongnu.org/archive/html/aeskulap-users/2018-11/msg00000.html)

#### インストール方法

```
% sudo apt install aeskulap
```


#### 起動方法

aeskulapコマンドを実行するとアプリケーションを起動できます。

```
% aeskulap
```


#### 医用画像の閲覧方法

メニューの[ファイル]-[開く]から対象のファイルを開くと閲覧できます。拡大といった操作はサポートされていません。

![aeskulapによる閲覧]({{ "/images/blog/20181113_0.png" | relative_url }} "aeskulapによる閲覧")

### pixelmed-apps

[PixelMed Publishing, LLC](http://www.pixelmed.com/)によって開発されているJava DICOM ToolkitにDICOMビューワーが含まれています。
公式には20181018が最新ですが、Debianでは20150917がpixelmed-appsとしてパッケージ化されています。
DICOMビューワーのほかにもいくつかツールがバンドルされています。

#### インストール方法

```
% sudo apt install pixelmed-apps
```


#### 起動方法

pixelmed-appsパッケージに含まれているdicomimageviewerコマンドを実行するとアプリケーションを起動できます。

```
% dicomimageviewer
```


#### 医用画像の閲覧方法

[Local]タブの[File]ボタンをクリックして対象のファイルを開くと閲覧できます。拡大といった操作はサポートされていません。

![dicomimageviewerによる閲覧]({{ "/images/blog/20181113_1.png" | relative_url }} "dicomimageviewerによる閲覧")

### DICOMscope

[OFFIS](https://dicom.offis.de/dscope.php.en)によって開発されているDICOMビューワーがDICOMscopeです。
公式には3.5.1が最新ですが、Debianでは3.6.0のパッケージが提供されています。

#### インストール方法

```
% sudo apt install dicomscope
```


#### 起動方法

dicomscopeコマンドを実行するとアプリケーションを起動できます。

```
% dicomscope
```


#### 医用画像の閲覧方法

[Load image file]ボタンをクリックして対象のファイルを開くと閲覧できます。Zoom機能があるので、細部を拡大してみてみたいときにはおすすめです。

![DICOMscopeによる閲覧]({{ "/images/blog/20181113_2.png" | relative_url }} "DICOMscopeによる閲覧")

### まとめ

今回は、Debianで医用画像を閲覧するためのアプリケーションを三つ紹介しました。
もし医用画像を閲覧する機会があれば（ないほうがいいのですが）ぜひ使ってみてください。
